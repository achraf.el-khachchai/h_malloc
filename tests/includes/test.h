#ifndef TEST_H
#define TEST_H

#include <assert.h>

#define IDENTIFY(lenght, values, results)    \
    {                                        \
        for (int i = 0; i < lenght; ++i)     \
            assert(values[i] == results[i]); \
    }

#define PARAMETRIZE(lenght, values, results, function) \
    {                                                  \
        for (int i = 0; i < lenght; ++i)               \
            assert(function(values[i]) == results[i]); \
    }

#endif /* TEST_H */