#include "test.h"

int id(int i)
{
    return i;
}

int main(void)
{
    int values[3] = {0, 1, 2};
    int results[3] = {0, 1, 2};

    IDENTIFY(3, values, results)
    PARAMETRIZE(3, values, results, id)
}