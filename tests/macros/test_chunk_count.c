#include "exp.h"
#include "test.h"

int main(void)
{
    unsigned long val[7] = {1, 7, 8, 9, 15, 16, 17};
    unsigned long res[7] = {1, 1, 1, 2, 2, 2, 3};
    PARAMETRIZE(7, val, res, CHUNK_COUNT)
}