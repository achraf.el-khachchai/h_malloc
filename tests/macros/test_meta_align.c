#include "exp.h"
#include "test.h"

int main(void)
{
    unsigned long val[7] = {1, 4079, 4080, 4081, 8175, 8176, 8177};
    unsigned long res[7] = {3, 512, 512, 513, 1024, 1024, 1025};
    PARAMETRIZE(7, val, res, META_ALIGN)
}