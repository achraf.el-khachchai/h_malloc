#include "exp.h"
#include "test.h"

int main(void)
{
    assert(CHUNK_SIZE == 8);
    assert(PAGE_SIZE == 4096);
    assert(PAGE_CHUNKS == 512);

    assert(FLAG_MASK == 0xFF00000000000000);
    assert(PREV_MASK == 0x00FFFFFFF0000000);
    assert(NEXT_MASK == 0x000000000FFFFFFF);

    return 0;
}