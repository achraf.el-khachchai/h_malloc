#include "exp.h"
#include "test.h"

size_t head = 0x0100000000000006;
size_t tail = 0x0000000060000000;

size_t x = 0x0000000000000002;
size_t y = 0x0100000020000003;
size_t z = 0x0000000030000000;

void test_1()
{
    size_t page[8] = {head, 0, 0, 0, 0, 0, 0, tail};
    free_list = &page[0];

    size_t *node = alloc_chunk(&page[0], 1);

    size_t a = 0x0100000000000004;
    size_t b = 0x0000000040000001;
    size_t c = 0x0000000010000000;
    size_t res[8] = {a, 0, 0, 0, 0, b, 0, c};

    IDENTIFY(8, page, res)
    assert(node == &page[5]);
    assert(free_list == &page[0]);
}

void test_2()
{
    size_t page[8] = {head, 0, 0, 0, 0, 0, 0, tail};
    free_list = &page[0];

    size_t *node = alloc_chunk(&page[0], 5);

    size_t a = 0x0000000000000006;
    size_t res[8] = {a, 0, 0, 0, 0, 0, 0, tail};

    IDENTIFY(8, page, res)
    assert(node == &page[0]);
    assert(!free_list);
}

void test_3()
{
    size_t page[8] = {head, 0, 0, 0, 0, 0, 0, tail};
    free_list = &page[0];

    size_t *node = alloc_chunk(&page[0], 6);

    size_t a = 0x0000000000000006;
    size_t res[8] = {a, 0, 0, 0, 0, 0, 0, tail};

    IDENTIFY(8, page, res)
    assert(node == &page[0]);
    assert(!free_list);
}

void test_4()
{
    size_t page[8] = {x, 0, 0, y, 0, 0, 0, z};
    free_list = &page[3];

    size_t *node = alloc_chunk(&page[3], 1);

    size_t a = 0x0000000000000002;
    size_t b = 0x0100000020000001;
    size_t c = 0x0000000010000001;
    size_t d = 0x0000000010000000;
    size_t res[8] = {a, 0, 0, b, 0, c, 0, d};

    IDENTIFY(8, page, res)
    assert(node == &page[5]);
    assert(free_list == &page[3]);
}

void test_5()
{
    size_t page[8] = {x, 0, 0, y, 0, 0, 0, z};
    free_list = &page[3];

    size_t *node = alloc_chunk(&page[3], 2);

    size_t a = 0x0000000000000002;
    size_t b = 0x0000000020000003;
    size_t c = 0x0000000030000000;
    size_t res[8] = {a, 0, 0, b, 0, 0, 0, c};

    IDENTIFY(8, page, res)
    assert(node == &page[3]);
    assert(!free_list);
}

void test_6()
{
    size_t page[8] = {x, 0, 0, y, 0, 0, 0, z};
    free_list = &page[3];

    size_t *node = alloc_chunk(&page[3], 3);

    size_t a = 0x0000000000000002;
    size_t b = 0x0000000020000003;
    size_t c = 0x0000000030000000;
    size_t res[8] = {a, 0, 0, b, 0, 0, 0, c};

    IDENTIFY(8, page, res)
    assert(node == &page[3]);
    assert(!free_list);
}

int main()
{
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    test_6();
}