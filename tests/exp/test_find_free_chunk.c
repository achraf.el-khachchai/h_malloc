#include "exp.h"
#include "test.h"

size_t chunk[8] = {
    0x0100000000000006, 0, 0, 0, 0, 0, 0, 0x0000000060000000};

void test_1()
{
    free_list = &chunk[0];

    assert(find_free_chunk(6) == &chunk[0]);
}

void test_2()
{
    size_t head = 0x0100000000000002;
    size_t tail = 0x0000000020000000;
    size_t page[4] = {head, ptr_to_addr(&chunk[0]), 0, tail};
    free_list = &page[0];

    assert(find_free_chunk(6) == &chunk[0]);
}

void test_3()
{
    size_t head = 0x0100000000000001;
    size_t nod1 = 0x0000000010000001;
    size_t nod2 = 0x0100000000000002;
    size_t tail = 0x0000000020000000;
    size_t page[8] = {head, 0, nod1, 0, nod2, 0, 0, tail};
    free_list = &page[0];
    page[1] = ptr_to_addr(&page[4]);

    assert(find_free_chunk(2) == &page[4]);
}

void test_4()
{
    free_list = &chunk[0];

    size_t *chunk = find_free_chunk(16);

    assert(!chunk);
}

void test_5()
{
    free_list = NULL;

    assert(!find_free_chunk(6));
}

int main()
{
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
}