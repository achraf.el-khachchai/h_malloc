#include "exp.h"
#include "test.h"

size_t head = 0x0100000000000002;
size_t tail = 0x0000000020000000;

void test_1()
{
    size_t chunk[4] = {head, 0, 0, tail};
    free_list = &chunk[0];

    pop_chunk_from_free_list(&chunk[0]);

    assert(!free_list);
}

void test_2()
{
    size_t next[4] = {0};
    size_t chunk[4] = {head, ptr_to_addr(&next[0]), 0, tail};
    free_list = &chunk[0];

    pop_chunk_from_free_list(&chunk[0]);

    assert(free_list == &next[0]);
}

void test_3()
{
    size_t chunk[4] = {head, 0, 0, tail};
    size_t prev[4] = {head, ptr_to_addr(&chunk[0]), 0, tail};
    free_list = &prev[0];

    pop_chunk_from_free_list(&chunk[0]);

    assert(free_list == &prev[0]);
    assert(!addr_to_ptr(prev[1]));
}

void test_4()
{
    size_t next[4] = {0};
    size_t chunk[4] = {head, ptr_to_addr(&next[0]), 0, tail};
    size_t prev[4] = {head, ptr_to_addr(&chunk[0]), 0, tail};
    free_list = &prev[0];

    pop_chunk_from_free_list(&chunk[0]);

    assert(free_list == &prev[0]);
    assert(addr_to_ptr(prev[1]) == &next[0]);
}

void test_5()
{
    size_t next[4] = {0};
    size_t chunk[4] = {head, 0, 0, tail};
    size_t prev[4] = {head, ptr_to_addr(&next[0]), 0, tail};
    free_list = &prev[0];

    pop_chunk_from_free_list(&chunk[0]);

    assert(free_list == &prev[0]);
    assert(addr_to_ptr(prev[1]) == &next[0]);
}

int main()
{
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
}