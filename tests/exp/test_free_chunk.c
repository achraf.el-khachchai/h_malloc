#include "exp.h"
#include "test.h"

size_t head = 0x0000000000000001;
size_t nod1 = 0x0000000010000002;
size_t nod2 = 0x0000000020000001;
size_t tail = 0x0000000010000000;

void test_1()
{
    size_t head = 0x0000000000000006;
    size_t tail = 0x0000000060000000;
    size_t page[8] = {head, 0, 0, 0, 0, 0, 0, tail};
    free_list = NULL;

    free_chunk(&page[0]);

    size_t res[8] = {head, 0, 0, 0, 0, 0, 0, tail};

    IDENTIFY(8, page, res)
    assert(!free_list);
}

void test_2()
{
    size_t page[8] = {head, 0, nod1, 0, 0, nod2, 0, tail};
    free_list = NULL;

    free_chunk(&page[2]);

    size_t node = 0x0100000010000002;
    size_t res[8] = {head, 0, node, 0, 0, nod2, 0, tail};

    IDENTIFY(8, page, res)
    assert(free_list == &page[2]);
}

void test_3()
{
    size_t free_head = 0x0100000000000001;
    size_t page[8] = {free_head, 0, nod1, 0, 0, nod2, 0, tail};
    free_list = &page[0];

    free_chunk(&page[2]);

    size_t a = 0x0100000000000004;
    size_t b = 0x0000000040000001;
    size_t res[8] = {a, 0, 0, 0, 0, b, 0, tail};

    IDENTIFY(8, page, res)
    assert(free_list == &page[0]);
}

void test_4()
{
    size_t free_nod2 = 0x0100000020000001;
    size_t page[8] = {head, 0, nod1, 0, 0, free_nod2, 0, tail};
    free_list = &page[5];

    free_chunk(&page[2]);

    size_t a = 0x0100000010000004;
    size_t b = 0x0000000040000000;
    size_t res[8] = {head, 0, a, 0, 0, 0, 0, b};

    IDENTIFY(8, page, res)
    assert(free_list == &page[2]);
}

void test_5()
{
    printf("test_5\n");
    size_t free_head = 0x0100000000000001;
    size_t free_nod2 = 0x0100000020000001;
    size_t page[8] = {free_head, 0, nod1, 0, 0, free_nod2, 0, tail};
    free_list = &page[0];
    page[1] = ptr_to_addr(&page[5]);

    free_chunk(&page[2]);

    size_t a = 0x0000000000000006;
    size_t b = 0x0000000060000000;
    size_t res[8] = {a, 0, 0, 0, 0, 0, 0, b};

    IDENTIFY(8, page, res)
    assert(!free_list);
}

void test_6()
{
    size_t free_head = 0x0100000000000001;
    size_t free_nod2 = 0x0100000020000001;
    size_t page[8] = {free_head, 0, nod1, 0, 0, free_nod2, 0, tail};
    free_list = &page[5];
    page[6] = ptr_to_addr(&page[0]);

    free_chunk(&page[2]);

    size_t a = 0x0000000000000006;
    size_t b = 0x0000000060000000;
    size_t res[8] = {a, 0, 0, 0, 0, 0, 0, b};

    IDENTIFY(8, page, res)
    assert(!free_list);
}

int main()
{
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    test_6();
}