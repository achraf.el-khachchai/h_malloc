#include "exp.h"
#include "test.h"

size_t chunk_head = 0x0100000000000002;
size_t chunk_tail = 0x0000000020000000;

void test_1()
{
    size_t head[4] = {chunk_head, 1, 0, chunk_tail};
    free_list = NULL;

    append_chunk_to_free_list(&head[0]);

    assert(free_list == &head[0]);
    assert(!addr_to_ptr(head[1]));
}

void test_2()
{
    size_t node[4] = {chunk_head, 1, 0, chunk_tail};
    size_t head[4] = {chunk_head, 0, 0, chunk_tail};
    free_list = &head[0];

    append_chunk_to_free_list(&node[0]);

    assert(free_list == &head[0]);
    assert(addr_to_ptr(head[1]) == &node[0]);
    assert(!addr_to_ptr(node[1]));
}

void test_3()
{
    size_t tail[4] = {chunk_head, 1, 0, chunk_tail};
    size_t node[4] = {chunk_head, 0, 0, chunk_tail};
    size_t head[4] = {chunk_head, ptr_to_addr(&node[0]), 0, chunk_tail};
    free_list = &head[0];

    append_chunk_to_free_list(&tail[0]);

    assert(free_list == &head[0]);
    assert(addr_to_ptr(head[1]) == &node[0]);
    assert(addr_to_ptr(node[1]) == &tail[0]);
    assert(!addr_to_ptr(tail[1]));
}

int main()
{
    test_1();
    test_2();
    test_3();
}