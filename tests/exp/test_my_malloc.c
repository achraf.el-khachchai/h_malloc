#include "exp.h"
#include "test.h"

void test_no_chunk_in_free_list()
{
    size_t *chunk = my_malloc(6 * CHUNK_SIZE);

    size_t *page_start = chunk - 505;
    size_t *chunk_head = chunk - 1;
    size_t *chunk_tail = chunk + 6;

    assert(*page_start == 0x01000000000001F7);
    assert(*chunk_head == 0x0000001F70000006);
    assert(*chunk_tail == 0x0000000060000000);
    assert(free_list == page_start);
}

void test_no_suitable_chunk_in_free_list()
{
    size_t head = 0x0100000000000004;
    size_t tail = 0x0000000040000000;
    size_t page[6] = {head, 0, 0, 0, 0, tail};
    free_list = &page[0];

    size_t *chunk = my_malloc(6 * CHUNK_SIZE);

    size_t *page_start = chunk - 505;
    size_t *chunk_head = chunk - 1;
    size_t *chunk_tail = chunk + 6;

    assert(*page_start == 0x01000000000001F7);
    assert(*chunk_head == 0x0000001F70000006);
    assert(*chunk_tail == 0x0000000060000000);
    assert(free_list == &page[0]);
    assert(addr_to_ptr(page[1]) == page_start);
}

void test_suitable_chunk_in_free_list()
{
    size_t head = 0x0100000000000006;
    size_t tail = 0x0000000060000000;
    size_t page[8] = {head, 0, 0, 0, 0, 0, 0, tail};
    free_list = &page[0];

    size_t *chunk = my_malloc(6 * CHUNK_SIZE);

    size_t *chunk_head = chunk - 1;
    size_t *chunk_tail = chunk + 6;
    size_t a = 0x0000000000000006;
    size_t b = 0x0000000060000000;
    size_t res[8] = {a, 0, 0, 0, 0, 0, 0, b};

    IDENTIFY(8, page, res)
    assert(chunk_head == &page[0]);
    assert(chunk_tail == &page[7]);
    assert(!free_list);
}

int main()
{
    test_no_chunk_in_free_list();
    test_no_suitable_chunk_in_free_list();
    test_suitable_chunk_in_free_list();
}