#include "exp.h"
#include "test.h"

int main()
{
    size_t head = 0x0000000000000006;
    size_t tail = 0x0000000060000000;
    size_t page[8] = {head, 0, 0, 0, 0, 0, 0, tail};
    free_list = NULL;

    my_free(&page[1]);

    size_t res[8] = {head, 0, 0, 0, 0, 0, 0, tail};

    IDENTIFY(8, page, res)
    assert(!free_list);
}