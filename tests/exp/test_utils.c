#include "exp.h"
#include "test.h"

int main(void)
{
    size_t value = 1;
    size_t *ptr = &value;
    size_t addr = ptr_to_addr(ptr);

    assert(addr_to_ptr(addr) == ptr);
}