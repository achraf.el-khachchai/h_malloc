#include "exp.h"
#include "test.h"

size_t head = 0x0100000000000006;
size_t tail = 0x0000000060000000;

void test_1()
{
    size_t chunk[8] = {head, 0, 0, 0, 0, 0, 0, tail};

    assert(split_chunk(&chunk[0], 5, 0, 1) == chunk);
}

void test_2()
{
    size_t chunk[8] = {head, 0, 0, 0, 0, 0, 0, tail};

    assert(split_chunk(&chunk[0], 6, 0, 1) == chunk);
}

void test_3()
{
    size_t chunk[8] = {head, 0, 0, 0, 0, 0, 0, tail};

    size_t *split = split_chunk(&chunk[0], 3, 0, 1);

    size_t res_head = 0x0000000000000003;
    size_t res_node = 0x0100000030000002;
    size_t res_tail = 0x0000000020000000;
    size_t res[8] = {res_head, 0, 0, 0, res_node, 0, 0, res_tail};

    assert(split == chunk + 4);
    IDENTIFY(8, chunk, res)
}

int main(void)
{
    test_1();
    test_2();
    test_3();
}