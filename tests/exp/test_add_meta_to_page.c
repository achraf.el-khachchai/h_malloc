#include "exp.h"
#include "test.h"

int main(void)
{
    size_t page[8] = {0};

    add_meta_to_page(&page[0], 8);

    size_t head = 0x0100000000000006;
    size_t tail = 0x0000000060000000;
    size_t res[8] = {head, 0, 0, 0, 0, 0, 0, tail};

    IDENTIFY(8, page, res)
    assert(free_list == &page[0]);
}