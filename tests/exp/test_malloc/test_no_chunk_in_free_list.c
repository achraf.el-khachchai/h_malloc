#include "exp.h"
#include "test.h"

int main(void)
{
    size_t *chunk = my_malloc(6 * CHUNK_SIZE);

    size_t *chunk_head = chunk - 1;
    size_t *chunk_tail = chunk + 6;

    assert(*chunk_head == 0x0000000000000006);
    assert(*chunk_tail == 0x01000000600001F7);
    assert(free_list == chunk_tail);
}