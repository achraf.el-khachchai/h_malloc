#include "exp.h"
#include "test.h"

int main(void)
{
    size_t head = 0x0100000000000004;
    size_t tail = 0x0000000040000000;
    size_t page[6] = {head, 0, 0, 0, 0, tail};
    free_list = &page[0];

    size_t *chunk = my_malloc(6 * CHUNK_SIZE);

    size_t *chunk_head = chunk - 1;
    size_t *chunk_tail = chunk + 6;

    assert(chunk_head != &page[0]);
    assert(*chunk_head == 0x0000000000000006);
    assert(*chunk_tail == 0x01000000600001F7);
    assert(free_list == &page[0]);
    printf("%p %p\n", (void *)(src_to_ref(&page[1])), (void *)chunk_tail);
    assert(src_to_ref(&page[1]) == chunk_tail);
}
