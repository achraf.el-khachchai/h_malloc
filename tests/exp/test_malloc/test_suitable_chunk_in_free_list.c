#include "exp.h"
#include "test.h"

int main(void)
{
    size_t head = 0x0100000000000006;
    size_t tail = 0x0000000060000000;
    size_t page[8] = {head, 0, 0, 0, 0, 0, 0, tail};
    free_list = &page[0];

    size_t *chunk = my_malloc(6 * CHUNK_SIZE);

    size_t *chunk_head = chunk - 1;
    size_t *chunk_tail = chunk + 6;

    assert(chunk_head == &page[0]);
    assert(chunk_tail == &page[7]);
    assert(*chunk_head == 0x0000000000000006);
    assert(*chunk_tail == 0x0000000060000000);
    assert(!free_list);
}
