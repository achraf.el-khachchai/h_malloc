#include "exp.h"
#include "test.h"

void test_1()
{
    size_t head = 0x0100000000000006;
    size_t tail = 0x0000000060000000;
    size_t page[8] = {head, 0, 2, 3, 4, 5, 0, tail};
    free_list = &page[0];

    size_t *chunk = my_calloc(3, 7);

    size_t a = 0x0100000000000002;
    size_t b = 0x0000000020000003;
    size_t c = 0x0000000030000000;
    size_t res[8] = {a, 0, 2, b, 0, 0, 0, c};

    IDENTIFY(8, page, res)
    assert(chunk == &page[4]);
    assert(free_list == &page[0]);
}

void test_2()
{
    size_t head = 0x0100000000000006;
    size_t tail = 0x0000000060000000;
    size_t page[8] = {head, 0, 2, 3, 4, 5, 0, tail};
    free_list = &page[0];

    size_t *chunk = my_calloc(3, 0xFFFFFFFFFFFFFFFF);

    assert(!chunk);
}

int main(void)
{
    test_1();
    test_2();
}