#include "exp.h"
#include "test.h"

int main(void)
{
    size_t head = 0x0100000000000003;
    size_t node = 0x0100000030000002;
    size_t tail = 0x0000000020000000;
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    concat_chunks(&page[0], &page[4]);

    size_t res_head = 0x0100000000000006;
    size_t res_tail = 0x0000000060000000;
    size_t res[8] = {res_head, 0, 0, 0, 0, 0, 0, res_tail};
    IDENTIFY(8, page, res);
}