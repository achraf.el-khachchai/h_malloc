#include "exp.h"
#include "test.h"

int main(void)
{
    free_list = NULL;

    size_t *c1 = my_malloc(200 * CHUNK_SIZE);
    size_t *c2 = my_malloc(200 * CHUNK_SIZE);
    size_t *c3 = my_malloc(300 * CHUNK_SIZE);
    size_t *c4 = my_malloc(100 * CHUNK_SIZE);
    my_free(c2);
    size_t *c5 = my_malloc(100 * CHUNK_SIZE);

    size_t *p1 = c4 - 9;
    size_t *t1 = c1 + 200;
    size_t *p2 = c5 - 110;
    size_t *t2 = c3 + 300;

    assert(c1 == c2 + 201);
    assert(c2 == c4 + 101);
    assert(c3 == c5 + 101);

    assert(free_list == p1);
    assert(addr_to_ptr(*(p1 + 1)) == p2);
    assert(addr_to_ptr(*(p2 + 1)) == c2 - 1);

    assert(*p1 == 0x0100000000000007);
    assert(*p2 == 0x010000000000006C);
    assert(*t1 == 0x0000000C80000000);
    assert(*t2 == 0x00000012C0000000);
    assert(*(c1 - 1) == 0x0000000C800000C8);
    assert(*(c2 - 1) == 0x01000006400000C8);
    assert(*(c3 - 1) == 0x000000064000012C);
    assert(*(c4 - 1) == 0x0000000070000064);
    assert(*(c5 - 1) == 0x00000006C0000064);

    my_free(c1);
    my_free(c4);

    assert(free_list == p2);
    assert(!addr_to_ptr(*(p2 + 1)));
}