#include "exp.h"
#include "test.h"

size_t head = 0x0100000000000003;
size_t node = 0x0100000030000002;
size_t tail = 0x0000000020000000;

void test_1()
{
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    size_t *chunk = resize_chunks(&page[0], &page[4], 7, 0, 0);

    size_t res[8] = {head, 0, 0, 0, node, 0, 0, tail};
    assert(chunk == &page[4]);
    IDENTIFY(8, page, res);
}

void test_2()
{
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    size_t *chunk = resize_chunks(&page[0], &page[4], 6, 0, 0);

    size_t res_head = 0x0100000000000006;
    size_t res_tail = 0x0000000060000000;
    size_t res[8] = {res_head, 0, 0, 0, 0, 0, 0, res_tail};
    assert(chunk == &page[0]);
    IDENTIFY(8, page, res);
}

void test_3()
{
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    size_t *chunk = resize_chunks(&page[0], &page[4], 5, 0, 0);

    size_t res_head = 0x0100000000000006;
    size_t res_tail = 0x0000000060000000;
    size_t res[8] = {res_head, 0, 0, 0, 0, 0, 0, res_tail};
    assert(chunk == &page[0]);
    IDENTIFY(8, page, res);
}

void test_4()
{
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    size_t *chunk = resize_chunks(&page[0], &page[4], 4, 1, 0);

    size_t res_head = 0x0100000000000004;
    size_t res_node = 0x0100000040000001;
    size_t res_tail = 0x0000000010000000;
    size_t res[8] = {res_head, 0, 0, 0, 0, res_node, 0, res_tail};
    assert(chunk == &page[5]);
    IDENTIFY(8, page, res);
}

void test_5()
{
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    size_t *chunk = resize_chunks(&page[0], &page[4], 1, 0, 0);

    size_t res_head = 0x0100000000000001;
    size_t res_node = 0x0000000010000004;
    size_t res_tail = 0x0000000040000000;
    size_t res[8] = {res_head, 0, res_node, 0, 0, 0, 0, res_tail};
    assert(chunk == &page[2]);
    IDENTIFY(8, page, res);
}

void test_6()
{
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    resize_chunks(&page[0], &page[4], 4, 1, 1);

    size_t res_head = 0x0100000000000001;
    size_t res_node = 0x0100000010000004;
    size_t res_tail = 0x0000000040000000;
    size_t res[8] = {res_head, 0, res_node, 0, 0, 0, 0, res_tail};
    IDENTIFY(8, page, res);
}

void test_7()
{
    size_t page[8] = {head, 0, 0, 0, node, 0, 0, tail};

    resize_chunks(&page[0], &page[4], 1, 0, 1);

    size_t res_head = 0x0100000000000004;
    size_t res_node = 0x0000000040000001;
    size_t res_tail = 0x0000000010000000;
    size_t res[8] = {res_head, 0, 0, 0, 0, res_node, 0, res_tail};
    IDENTIFY(8, page, res);
}

int main(void)
{
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    test_6();
    test_7();
}