#include "exp.h"

size_t *free_list = NULL;

void *my_malloc(size_t size)
{
    if (!size)
        return NULL;

    size_t chunk_count = CHUNK_COUNT(size);
    size_t *chunk = find_free_chunk(chunk_count);

    if (!chunk)
    {
        size_t length = PAGE_COUNT(size) * PAGE_SIZE;
        int prot = PROT_READ | PROT_WRITE;
        int flags = MAP_ANONYMOUS | MAP_PRIVATE;
        chunk = mmap(NULL, length, prot, flags, -1, 0);

        if (chunk == MAP_FAILED)
            return NULL;

        add_meta_to_page(chunk, CHUNK_COUNT(length));
    }

    return alloc_chunk(chunk, chunk_count) + 1;
}

void my_free(void *ptr)
{
    if (!ptr)
        return;

    size_t *chunk_to_free = ptr;
    free_chunk(chunk_to_free - 1);
}

void *my_calloc(size_t number, size_t size)
{
    size_t chunk_size = number * size;

    if (!chunk_size || chunk_size < size || chunk_size < number)
        return NULL;

    void *chunk = my_malloc(chunk_size);
    chunk = memset(chunk, 0, chunk_size);

    return chunk;
}

void *my_realloc(void *ptr, size_t size)
{
    if (!ptr)
        return my_malloc(size);

    if (!size)
    {
        my_free(ptr);
        return NULL;
    }

    size_t count = CHUNK_COUNT(size);
    size_t *chunk = ptr;
    chunk = chunk - 1;

    if (count > NEXT(*chunk))
    {
        size_t *res = realloc_chunk(chunk, count);
        if (!res)
        {
            res = my_malloc(size);
            if (res)
            {
                memcpy(res, chunk + 1, NEXT(*chunk));
                free_chunk(chunk);
            }
        }

        chunk = res;
    }
    else
    {
        size_t *node = split_chunk(chunk, count, 0, 1);
        if (node != chunk)
            append_chunk_to_free_list(node);
    }

    return chunk + 1;
}

void add_meta_to_page(size_t *page, size_t chunk_count)
{
    if (!page)
        return;

    *page = META(1UL, 0UL, chunk_count - 2);
    *(page + chunk_count - 1) = META(0UL, chunk_count - 2, 0);
    append_chunk_to_free_list(page);
}

size_t *alloc_chunk(size_t *chunk, size_t chunk_count)
{
    if (!chunk || chunk_count > NEXT(*chunk))
        return chunk;

    size_t size = NEXT(*chunk) - chunk_count;
    size = size ? size - 1 : 0;
    size_t *node = split_chunk(chunk, size, FLAG(*chunk), 0UL);

    if (node == chunk)
        pop_chunk_from_free_list(chunk);

    return node;
}

size_t *realloc_chunk(size_t *chunk, size_t size)
{
    size_t *curr = chunk;
    size_t *prev = chunk - PREV(*chunk) - 1;
    size_t *next = chunk + NEXT(*chunk) + 1;
    size_t curr_size = NEXT(*curr);
    size_t prev_size = NEXT(*prev) && FLAG(*prev);
    size_t next_size = NEXT(*next) && FLAG(*next);

    if (curr_size + prev_size + 1 >= size)
    {
        chunk = resize_chunks(prev, chunk, size, 0, 1);
        if (prev == chunk)
            pop_chunk_from_free_list(prev);
        chunk = memcpy(chunk + 1, curr + 1, curr_size);
    }
    else if (curr_size + next_size + 1 >= size)
    {
        pop_chunk_from_free_list(next);
        size_t *left = resize_chunks(chunk, next, size, 0, 0);
        if (left != chunk)
            append_chunk_to_free_list(left);
    }
    else if (curr_size + prev_size + next_size + 2 >= size)
    {
        pop_chunk_from_free_list(next);
        resize_chunks(chunk, next, curr_size + next_size + 1, 0, 0);
        chunk = resize_chunks(prev, chunk, size, 0, 1);
        if (prev == chunk)
            pop_chunk_from_free_list(prev);
        chunk = memcpy(chunk + 1, curr + 1, curr_size);
    }
    else
        return NULL;

    return chunk;
}

void free_chunk(size_t *chunk_to_free)
{
    if (!chunk_to_free)
        return;

    int is_free = 0;
    size_t *chunk = chunk_to_free;
    size_t *prev = PREV(*chunk) ? chunk - PREV(*chunk) - 1 : NULL;
    size_t *next = chunk + NEXT(*chunk) + 1;

    if (prev && FLAG(*prev))
    {
        concat_chunks(prev, chunk);
        chunk = prev;
        is_free = 1;
    }

    if (NEXT(*next) && FLAG(*next))
    {
        pop_chunk_from_free_list(next);
        concat_chunks(chunk, next);
    }

    if (!PREV(*chunk) && !NEXT(*next))
    {
        pop_chunk_from_free_list(chunk);
        munmap(chunk, (NEXT(*chunk) + 2) * CHUNK_SIZE);
    }
    else if (!is_free)
    {
        *chunk = META(1UL, PREV(*chunk), NEXT(*chunk));
        append_chunk_to_free_list(chunk);
    }
}

size_t *find_free_chunk(size_t chunk_count)
{
    size_t *node = free_list;

    while (node && NEXT(*node) < chunk_count)
        node = addr_to_ptr(*(node + 1));

    return node;
}

void append_chunk_to_free_list(size_t *chunk)
{
    // if (!chunk)
    if (!(chunk && FLAG(*chunk)))
        return;

    *(chunk + 1) = 0;

    if (free_list)
    {
        size_t *node = free_list;

        while (addr_to_ptr(*(node + 1)))
            node = addr_to_ptr(*(node + 1));

        *(node + 1) = ptr_to_addr(chunk);
    }
    else
        free_list = chunk;

    // *chunk = META(1UL, PREV(*chunk), NEXT(*chunk));
}

void pop_chunk_from_free_list(size_t *chunk)
{
    if (!(free_list && chunk))
        return;

    if (free_list == chunk)
        free_list = addr_to_ptr(*(chunk + 1));
    else
    {
        size_t *node = free_list;

        while (node && addr_to_ptr(*(node + 1)) != chunk)
            node = addr_to_ptr(*(node + 1));

        if (node)
            *(node + 1) = *(chunk + 1);
    }

    *chunk = META(0UL, PREV(*chunk), NEXT(*chunk));
    *(chunk + 1) = 0;
}

size_t *resize_chunks(size_t *head, size_t *node, size_t size, size_t flag, int left)
{
    if (!head || !node || size > PREV(*node) + NEXT(*node) + 1)
        return node;

    size_t max_size = PREV(*node) + NEXT(*node) + 1;

    concat_chunks(head, node);
    if (size >= max_size - 1)
        return head;

    size = left ? max_size - size - 1 : size;
    return split_chunk(head, size, FLAG(*head), flag);
}

void concat_chunks(size_t *head, size_t *node)
{
    if (!(head && node))
        return;

    size_t *tail = node + NEXT(*node) + 1;
    size_t size = PREV(*node) + NEXT(*node) + 1;

    *node = 0;
    *head = META(FLAG(*head), PREV(*head), size);
    *tail = META(FLAG(*tail), size, NEXT(*tail));
}

size_t *split_chunk(size_t *chunk, size_t size, size_t chunk_flag, size_t node_flag)
{
    if (!chunk || !size || size >= NEXT(*chunk) - 1)
        return chunk;

    size_t chunk_size = NEXT(*chunk);
    size_t *node = chunk + size + 1;
    size_t *tail = chunk + chunk_size + 1;

    *chunk = META(chunk_flag, PREV(*chunk), size);
    *node = META(node_flag, size, chunk_size - size - 1);
    *tail = META(FLAG(*tail), chunk_size - size - 1, NEXT(*tail));

    return node;
}

size_t ptr_to_addr(size_t *ptr)
{
    if (!ptr)
        return 0;

    void *tmp = &ptr;
    size_t *addr = tmp;
    return *addr;
}

size_t *addr_to_ptr(size_t addr)
{
    if (!addr)
        return NULL;

    void *tmp = &addr;
    size_t **ptr = tmp;
    return *ptr;
}
