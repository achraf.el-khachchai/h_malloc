#ifndef EXP_H
#define EXP_H

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

/* MACROS */

#define CHUNK_SIZE (sizeof(size_t))
#define PAGE_SIZE (sysconf(_SC_PAGE_SIZE))
#define PAGE_CHUNKS (PAGE_SIZE / CHUNK_SIZE)

#define FLAG_MASK (0xFF00000000000000)
#define PREV_MASK (0x00FFFFFFF0000000)
#define NEXT_MASK (0x000000000FFFFFFF)

#define CHUNK_COUNT(size) \
    ((size) / CHUNK_SIZE + !!((size) % CHUNK_SIZE))
#define META_ALIGN(size) \
    (CHUNK_COUNT(size) + 2)
#define PAGE_COUNT(size) \
    (META_ALIGN(size) / PAGE_CHUNKS + !!(META_ALIGN(size) % PAGE_CHUNKS))

#define FLAG(meta) \
    (((meta) & (FLAG_MASK)) >> 56)
#define PREV(meta) \
    (((meta) & (PREV_MASK)) >> 28)
#define NEXT(meta) \
    ((meta) & (NEXT_MASK))
#define META(flag, prev, next) \
    (((flag) << 56) | ((prev) << 28) | (next))

/* VARIABLES */

extern size_t *free_list;

/* LIB */

void *my_malloc(size_t size);
void my_free(void *ptr);
void *my_calloc(size_t number, size_t size);
void *my_realloc(void *ptr, size_t size);

/* FUNCTIONS */

/**
 * @brief Add meta data to the page.
 */
void add_meta_to_page(size_t *page, size_t chunk_count);

/**
 * @brief Alloc a chunk from the free_list.
 */
size_t *alloc_chunk(size_t *chunk, size_t chunk_count);

/**
 * @brief Alloc a chunk from the free_list.
 */
size_t *realloc_chunk(size_t *chunk, size_t chunk_count);

/**
 * @brief Free a chunk into the free_list.
 */
void free_chunk(size_t *chunk);

/**
 * @brief Look for a suitable chunk in the free_list.
 */
size_t *find_free_chunk(size_t chunk_count);

/* HELPERS */

void append_chunk_to_free_list(size_t *chunk);
void pop_chunk_from_free_list(size_t *chunk);
size_t *resize_chunks(size_t *head, size_t *node, size_t size, size_t flag, int left);
void concat_chunks(size_t *head, size_t *node);
size_t *split_chunk(size_t *chunk, size_t size, size_t chunk_flag, size_t node_flag);

/* UTILS */

/**
 * @brief Returns the address of a pointer.
 */
size_t ptr_to_addr(size_t *ptr);

/**
 * @brief Returns the pointer of an address.
 */
size_t *addr_to_ptr(size_t adr);

#endif /* EXP_H */
