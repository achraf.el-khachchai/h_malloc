#include "exp.h"
#include "lib.h"

__attribute__((__visibility__("default"))) void *malloc(size_t size)
{
    return my_malloc(size);
}

__attribute__((__visibility__("default"))) void free(void *ptr)
{
    my_free(ptr);
}

__attribute__((__visibility__("default"))) void *calloc(size_t number, size_t size)
{
    return my_calloc(number, size);
}

__attribute__((__visibility__("default"))) void *realloc(void *ptr, size_t size)
{
    return my_realloc(ptr, size);
}